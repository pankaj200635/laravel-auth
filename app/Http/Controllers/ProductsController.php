<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
    //

    function save(Request $req){
        $req->validate([
            'name' => 'required|string',
            'category' => 'required|string',
            'price' => 'required|string'
        ]);

        // $credentials = request(['name', 'category','price']);
        // if(!Auth::attempt($credentials))
        //     return response()->json([
        //         'message' => 'Unauthorized'
        //     ], 401);

        $product=new Product;
        $product->name=$req->name;
        $product->category=$req->category;
        $product->price=$req->price;
        return $product->save();
    }

    function update(Request $req){
        $product=Product::find($req->id);
        $product->category= $req->category;
if($product->save()){
    return ['Result'=>'success','msg'=>"data is updated"];


}



    }
}
